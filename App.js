
import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";

import Screen1 from "./Src/screens/Screen1";
import BottomTab from "./Src/screens/BottomTab";
import {LogBox} from 'react-native'
// Ignore log notification by message:
LogBox.ignoreLogs(['Warning: ...']);

// Ignore all log notifications:
LogBox.ignoreAllLogs();

import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { View, Text, Pressable, TouchableOpacity } from "react-native";

const App = () => {
  const Stack = createNativeStackNavigator();
  const [hideSplashScreen, setHideSplashScreen] = React.useState(true);

  
  return (
    <>
      <NavigationContainer>
        {hideSplashScreen ? (
          <Stack.Navigator screenOptions={{ headerShown: false }}>
             <Stack.Screen
              name="Screen1"
              component={Screen1}
              options={{ headerShown: false }}
            />
             <Stack.Screen
              name="BottomTab"
              component={BottomTab}
              options={{ headerShown: false }}
            />
          </Stack.Navigator>
        ) : null}
      </NavigationContainer>
    </>
  );
};
export default App;

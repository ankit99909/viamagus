//import liraries
import React from 'react';
import {View,Image} from 'react-native';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Screen3 from '../screens/Screen3';





const Tab = createBottomTabNavigator();

const BottomTab = ({route}) => {

  return (
    <Tab.Navigator
    screenOptions={{
      animationEnabled: false
    }}

    tabBarOptions={{
      showIcon: true ,
      showLabel:true,
      activeTintColor: "#6231AD",
      inactiveTintColor: "#727682",
      tabStyle:{backgroundColor:"#ffffff",height:44, },
      labelStyle: { fontSize: 11, fontWeight: 'bold'}
       }}>
      <Tab.Screen
        name="Home"
        component={Screen3}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size, focused}) => (
            <Image
            style={{width:"37%",height:"60%"}}
            // contentFit="cover"
            tintColor={color}
            source={require("../../assets/home.png")}
          />  
          ),
        }}
      />
  <Tab.Screen
        name="Leagues"
        component={Screen3}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size, focused}) => (
            <Image
            style={{width:"32%",height:"70%"}}
            tintColor={color}
            source={require("../../assets/Vector.png")}
          />  
          ),
        }}
      />
       <Tab.Screen
        name="Research"
        component={Screen3}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size, focused}) => (
            <Image
            style={{width:"36%",height:"75%"}}
            tintColor={color}
            source={require("../../assets/Frame12281.png")}
          />  
          ),
        }}
      />
        <Tab.Screen
        name="Leaderborad"
        component={Screen3}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size, focused}) => (
            <Image
            style={{width:"38%",height:"72%"}}
            tintColor={color}
            source={require("../../assets/Group12493.png")}
          />  
          ),
        }}
      />
       <Tab.Screen
        name="Profile"
        component={Screen3}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size, focused}) => (
            <Image
            style={{width:"24%",height:"68%"}}
            tintColor={color}
            source={require("../../assets/profilepic.png")}
          />  
          ),
        }}
      />
    </Tab.Navigator>
  );
};

// define your styles

//make this component available to the app
export default BottomTab;

import * as React from "react";
import { StyleSheet, Text, View,StatusBar ,Image} from "react-native";
import PredictionForm from "../components/PredictionForm";
import FormContainer from "../components/FormContainer";
import PlayerSection from "../components/PlayerSection";
import { Color, FontFamily, FontSize, Border } from "../components/GlobalStyles";

const Screen1 = ({ toggleModal }) => {
  return (
    <View style={styles.screen1}>
        <StatusBar
        animated={true}
         backgroundColor="#ffffff"
        barStyle={"dark-content"}
      />
    <Image
        style={[styles.rectangleIcon, styles.iconLayout]}
        contentFit="cover"
        source={require("../../assets/rectangle5.png")}
      />  
    <View style={styles.tonksParent}>
        <Text style={[styles.tonks, styles.tonksFlexBox]}>$tonks</Text>
        <Text style={[styles.text, styles.textTypo]}>1000</Text>
      </View> 
     <Image 
        style={[styles.iconfontAwesomeFreesolidc, styles.iconLayout]}
        contentFit="cover"
        source={require("../../assets/iconfont-awesome-freesolidccommentalt2.png")}
      />
      <View style={styles.ovalParent}>
        <Image
          style={[styles.ovalIcon, styles.iconLayout]}
          contentFit="cover"
          source={require("../../assets/oval2.png")}
        />
         <Text style={[styles.text1, styles.tonksFlexBox]}>1</Text>
      </View>
      <Image
        style={[styles.screen1Child, styles.todaysGamesPosition]}
        contentFit="cover"
        source={require("../../assets/group-113452.png")}
      />
      <Image
        style={[styles.mask29Icon, styles.iconLayout]}
        contentFit="cover"
        source={require("../../assets/mask-291.png")}
      />
      <View style={styles.iconfontAwesomeFreesolidc1} />
      {/* <GreetingSection /> */}
      <Text style={[styles.todaysGames, styles.todaysGamesPosition]}>
        Today’s Games
      </Text>
      <Image
        style={styles.iphoneX11ProLightBas}
        contentFit="cover"
        source={require("../../assets/iphone-x-11-pro--light--base1.png")}
      />
      <Image
        style={[styles.icon, styles.iconLayout]}
        contentFit="cover"
        source={require("../../assets/254981.png")}
      />
      <View style={[styles.groupParent, styles.groupParentLayout]}>
        <View style={[styles.rectangleParent, styles.groupParentLayout]}>
          <View style={[styles.groupChild, styles.groupParentLayout]} />
          <PredictionForm  />
          <FormContainer />
        </View>
       <PlayerSection />
       
      </View> 
    </View>
  );
};

const styles = StyleSheet.create({
  iconLayout: {
    maxHeight: "100%",
    maxWidth: "100%",
    overflow: "hidden",
  },
  tonksFlexBox: {
    textAlign: "left",
    position: "absolute",
  },
  textTypo: {
    color: Color.primaryColor,
    textAlign: "left",
    fontFamily: FontFamily.avenirNext,
  },
  todaysGamesPosition: {
    left: "4.27%",
    position: "absolute",
  
  },
  groupParentLayout: {
    height: 389,
    width: 343,
    position: "absolute",
  },
  rectangleIcon: {
    height: "20%",
    width: "82.93%",
    top: "145.94%",
    right: "8.53%",
    bottom: "-65.94%",
    left: "8.53%",
    position: "absolute",
  },
  tonks: {
    left: 39,
    fontWeight: "500",
    color: Color.secondaryColro,
    fontFamily: FontFamily.avenirNext,
    textAlign: "left",
    lineHeight: 26,
    fontSize: FontSize.size_sm,
    top: 0,
  },
  text: {
    left: 0,
    lineHeight: 26,
    fontSize: FontSize.size_sm,
    color: Color.primaryColor,
    top: 0,
    position: "absolute",
  },
  tonksParent: {
    top: -229,
    left: 219,
    width: 83,
    height: 26,
    position: "absolute",
  },
  iconfontAwesomeFreesolidc: {
    height: "2.91%",
    width: "6.4%",
    top: "-27.64%",
    right: "5.6%",
    bottom: "124.73%",
    left: "88%",
    position: "absolute",
  },
  ovalIcon: {
    height: "100%",
    top: "0%",
    right: "0%",
    bottom: "0%",
    left: "0%",
    position: "absolute",
    width: "100%",
  },
  text1: {
    top: "12.5%",
    left: "31.25%",
    fontSize: FontSize.size_2xs,
    fontFamily: FontFamily.helvetica,
    color: Color.colorWhite,
  },
  ovalParent: {
    height: "1.94%",
    width: "4.27%",
    top: "-28.73%",
    right: "4.27%",
    bottom: "126.79%",
    left: "91.47%",
    position: "absolute",
  },
  screen1Child: {
    height: "3.64%",
    width: "6.93%",
    top: "-28%",
    right: "88.8%",
    bottom: "124.36%",
    maxHeight: "100%",
    maxWidth: "100%",
    overflow: "hidden",
  },
  mask29Icon: {
    height: "1.45%",
    width: "4.8%",
    top: "4.73%",
    right: "21.6%",
    bottom: "93.82%",
    left: "73.6%",
    position: "absolute",
  },
  iconfontAwesomeFreesolidc1: {
    top: 305,
    left: 33,
    width: 20,
    height: 20,
    position: "absolute",
    overflow: "hidden",
  },
  todaysGames: {
    top: "1.58%",
    fontSize: FontSize.size_base,
    color: Color.primaryColor,
    textAlign: "left",
    fontFamily: FontFamily.avenirNext,
    fontWeight:'bold'
  },
  iphoneX11ProLightBas: {
    top: -300,
    width: 375,
    height: 44,
    left: 0,
    position: "absolute",
    overflow: "hidden",
  },
  icon: {
    height: "1.58%",
    width: "3.47%",
    top: "-27.03%",
    right: "42.4%",
    bottom: "125.45%",
    left: "54.13%",
    position: "absolute",
  },
  groupChild: {
    borderRadius: Border.br_8xs,
    borderStyle: "solid",
    borderColor: Color.colorGhostwhite_200,
    borderWidth: 1,
    left: 0,
    top: 0,
    width: 343,
    backgroundColor: Color.colorWhite,
  },
  rectangleParent: {
    left: 0,
    top: 0,
    width: 343,
  },
  groupParent: {
    top: 51,
    left: 16,
    shadowColor: "rgba(0, 0, 0, 0.25)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 4,
    elevation: 4,
    shadowOpacity: 1,
  },
  screen1: {
    flex: 1,
    height: 825,
    overflow: "hidden",
    width: "100%",
    backgroundColor: Color.colorWhite,
  },
});

export default Screen1;

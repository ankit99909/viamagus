import * as React from "react";
import { StyleSheet, View, Text,FlatList,TouchableOpacity,Image } from "react-native";
import SectionForm from "../components/SectionForm";
import SectionCard from "../components/SectionCard";
import { Color, Border, FontFamily, FontSize } from "../components/GlobalStyles";


const Screen3 = () => {
  const [change,setchange]=React.useState("true")

  const DATA = [
    {
      title: ' First Stripe Earned',
      x :"x 3",
      SubTitle:' Top 10% of highest spending players in a month',
      Image : require("../../assets/group-12664.png"),
    },  
    {
      title: ' Born Winner',
      SubTitle:'Top 10% of highest spending players in a month',
      Image : require("../../assets/group-12664.png"),
    },  
    {
      title: 'Trader of the Month',
      SubTitle:'Won 7 under-over games in a row',
      Image : require("../../assets/group-12664.png"),
    },  
    {
      title: ' Rising Star',
      SubTitle:' Top 10% of highest spending players in a month',
      Image : require("../../assets/group-12664.png"),
    },  
    {
      title: 'Impossible',
      SubTitle:' Top 10% of highest spending players in a month',
      Image : require("../../assets/group-12664.png"),
    },  
    {
      title: 'Jackpot',
      SubTitle:' Top 10% of highest spending players in a month',
      Image : require("../../assets/group-12664.png"),
    },  
  ];

  return (
    <View style={styles.screen3}>
      <View style={styles.screen3Child} />
      <Text style={styles.profile}>Profile</Text>
      <View style={[styles.groupParent, styles.groupPosition]}>
        <SectionForm />
        <View style={[styles.groupContainer, styles.groupPosition]}>
          <View style ={{top:70,flex:1,backgroundColor:"#f7faf8"}}>
            <View style={{height:200,left:10}}>
{change == "nocolorChnage"?(
              <FlatList
        data={DATA}
        renderItem={({item}) =>{
          return(
            <View style={{flex:1}}>
                <View style={[styles.rectangleView, styles.rectangleLayout]} />
                <View style={[styles.groupFrame, styles.frameLayout]}>
                  <View style={[styles.frameParent, styles.frameLayout]}>
                    <View style={styles.firstStripeEarnedGroup}>
                      <Text
                        style={[styles.firstStripeEarned1, styles.firstTypo]}
                      >
                      {item?.title}
                      </Text>
                      <Text style={[styles.x3, styles.x3Typo]}>{item.x}</Text>
                    </View>
                    <Text style={[styles.top10Of1, styles.top10Typo]}>
                    {item.SubTitle}
                    </Text>
                  </View>
                </View>
                <Image
                  style={[styles.groupIcon, styles.groupLayout]}
                  contentFit="cover"
                  source={item.Image}
                />
              </View> 
          )
        }
      }
      />
  ):(
    null

  )}
            </View>
           </View> 
          <View style={[styles.gamesPlayedParent, styles.lightLayout]}>
            <TouchableOpacity onPress={()=>setchange("colorChange")}>
            <Text style={[styles.gamesPlayed, styles.badgesPosition,{color:change =="nocolorChnage"?Color.secondaryColro:Color.colorIndigo }]}>
              Games played
            </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>setchange("nocolorChnage")}>
            <Text style={[styles.badges, styles.badgesPosition,{color:change =="nocolorChnage"? Color.colorIndigo:Color.secondaryColro}]}>Badges</Text>
            </TouchableOpacity>
            <View style={[styles.groupChild1, styles.groupChildBg1,{  borderBottomWidth:change =="nocolorChnage"? 2:0,},]} />
            <View style={[styles.groupChild2, styles.groupChildBg,{  borderBottomWidth:change =="nocolorChnage"?0:2}]} />
          </View>
        </View>
        <View
          style={[styles.iconfontAwesomeFreesolide, styles.frameIconLayout]}
        />
      </View>
      <Image
        style={[styles.screen3Item, styles.ovalIconLayout]}
        contentFit="cover"
        source={require("../../assets/group-12465.png")}
      />
      <View style={styles.iconfontAwesomeFreesolidcParent}>
        <Image
          style={[styles.iconfontAwesomeFreesolidc, styles.ovalIconLayout]}
          contentFit="cover"
          source={require("../../assets/iconfont-awesome-freesolidccommentalt.png")}
        />
        <View style={[styles.ovalParent, styles.ovalParentPosition]}>
          <Image
            style={[styles.ovalIcon, styles.ovalIconLayout]}
            contentFit="cover"
            source={require("../../assets/oval.png")}
          />
          <Text style={styles.text}>1</Text>
        </View>
      </View>
      <View style={[styles.logoutParent, styles.frameIconLayout]}>
        <Text style={[styles.logout, styles.top10Typo]}>Logout</Text>
        <Image
          style={[styles.frameIcon, styles.frameIconLayout]}
          contentFit="cover"
          source={require("../../assets/frame-12282.png")}
        />
      </View>
      <View style={[styles.homeIndicatorLight, styles.lightLayout]}>
        <View style={styles.homeIndicator} />
      </View>
      <SectionCard />
    </View>
  );
};

const styles = StyleSheet.create({
  lightLayout: {
    width: 375,
    position: "absolute",
  },
  groupPosition: {
    width: 376,
    left: 0,
    position: "absolute",
  },
  groupChildLayout: {
    height: 950, // Fixed height
    width: 390,
    left: 0,
    backgroundColor:"#e3e1e1"
   
  },
  groupChildBg: {
    borderBottomWidth:2,
    borderColor: Color.colorIndigo,
    // backgroundColor: Color.colorIndigo,
    position: "absolute",
    marginLeft:20
  },
  groupChildBg1: {
    borderBottomWidth:2,
    borderColor: Color.colorIndigo
    // backgroundColor: Color.colorIndigo,
    // position: "absolute",
  },
  groupLayout1: {
    height: 95,
    width: 343,
    position: "absolute",
  },
  groupItemBorder: {
    borderWidth: 1,
    borderColor: Color.colorGhostwhite_200,
    borderStyle: "solid",
    borderRadius: Border.br_8xs,
    left: 0,
    backgroundColor: Color.colorWhite,
  },
  ovalIconPosition: {
    bottom: "0%",
    left: "0%",
  },
  firstTypo: {
    color: Color.primaryColor,
    textAlign: "left",
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
    fontSize: FontSize.size_sm,
  },
  top10Typo: {
    color: Color.secondaryColro,
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
  },
  groupLayout: {
    height: 60,
    width: 60,
    left: 16,
    position: "absolute",
  },
  rectangleLayout: {
    height: 105,
    width: 343,
    top: 0,
    // position: "absolute",
    margin:10,
  },
  frameLayout: {
    height: 65,
    width: 231,
    position: "absolute",
  },
  x3Typo: {
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
  },
  badgesPosition: {
    top: 24,
    textAlign: "center",
    fontSize: FontSize.size_sm,
    position: "absolute",
  },
  frameIconLayout: {
    height: 20,
    position: "absolute",
  },
  ovalIconLayout: {
    maxHeight: "100%",
    maxWidth: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  ovalParentPosition: {
    right: "0%",
    top: "0%",
  },
  iphoneX11ProLightBas: {
    height: 44,
    left: 0,
    top: 0,
    overflow: "hidden",
  },
  screen3Child: {
    top: 1508,
    left: -1595,
    backgroundColor: "#c4c4c4",
    width: 29,
    height: 16,
    position: "absolute",
  },
  profile: {
    top: 75,
    left: 164,
    textAlign: "center",
    color: Color.secondaryColro,
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    position: "absolute",
  },
  groupChild: {
    opacity: 0.06,
    height: 950,
    width: 375,
    left: 0,
    top: 0,
  },
  groupItem: {
    height: 95,
    width: 343,
    position: "absolute",
    top: 0,
  },
  firstStripeEarned: {
    textAlign: "left",
    left: "0%",
    top: "0%",
    color: Color.primaryColor,
    position: "absolute",
  },
  top10Of: {
    top: "44.26%",
    textAlign: "left",
    left: "0%",
    fontSize: FontSize.size_sm,
    position: "absolute",
    width: "100%",
  },
  firstStripeEarnedParent: {
    left: "0%",
    right: "0%",
    top: "0%",
    height: "100%",
    bottom: "0%",
    width: "100%",
    position: "absolute",
  },
  groupWrapper: {
    height: "64.21%",
    width: "67.35%",
    top: "16.84%",
    right: "5.83%",
    bottom: "18.95%",
    left: "26.82%",
    position: "absolute",
  },
  groupInner: {
    top: 18,
  },
  rectangleGroup: {
    top: 121,
    left: 0,
  },
  groupParent1: {
    top: 606,
    height: 216,
    width: 343,
    left: 0,
    position: "absolute",
  },
  rectangleView: {
    borderWidth: 1,
    borderColor: Color.colorGhostwhite_200,
    borderStyle: "solid",
    borderRadius: Border.br_8xs,
    left: 0,
    backgroundColor: Color.colorWhite,
  },
  firstStripeEarned1: {
    textAlign: "left",
  },
  x3: {
    color: Color.goldTextColor,
    marginLeft: 4,
    textAlign: "left",
    fontSize: FontSize.size_sm,
  },
  firstStripeEarnedGroup: {
    flexDirection: "row",
    left: 0,
    top: 0,
    position: "absolute",
  },
  top10Of1: {
    top: "38.46%",
    lineHeight: 20,
    textAlign: "left",
    left: "0%",
    fontSize: FontSize.size_sm,
    position: "absolute",
    width: "100%",
  },
  frameParent: {
    left: 0,
    top: 0,
  },
  groupFrame: {
    left: 92,
    top: 20,
  },
  groupIcon: {
    top: 20,
  },
  rectangleContainer: {
    // flex:1,
    // left: 1,
    margin:10,
  },
  groupView: {
    left: 15,
    width: 360,
    height: 822,
    top: 20,
    position: "absolute",
    backgroundColor:" #6231AD"
  },
  rectangleParent: {
    top: 64,
    position: "absolute",
   
  },
  gamesPlayed: {
    left: 43,
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
  },
  badges: {
    left: 256,
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
  },
  groupChild1: {
    top: 62,
     left: 188,
    width: 188,
    height: 2,
  },
  groupChild2: {
    top: 62,
    right: 188,
    width: 188,
    height: 2,
  },
  gamesPlayedParent: {
    height: 64,
    left: 1,
    top: 0,
  },
  groupContainer: {
    top: 194,
    height: 1014,
  },
  iconfontAwesomeFreesolide: {
    width: 20,
    height: 20,
    top: 0,
    overflow: "hidden",
    left: 16,
  },
  groupParent: {
    top: 358,
    height: 1208,
  },
  screen3Item: {
    height: "4.01%",
    width: "7.95%",
    top: "4.61%",
    right: "87.79%",
    bottom: "93.37%",
    left: "4.27%",
  },
  iconfontAwesomeFreesolidc: {
    height: "140.73%",
    width: "100.76%",
    top: "27.27%",
    right: "17.24%",
    left: "0%",
    bottom: "0%",
  },
  ovalIcon: {
    left: "0%",
    bottom: "0%",
    right: "0%",
    top: "0%",
    height: "200%",
    width: "110%",
  },
  text: {
    top: "12.5%",
    left: "31.25%",
    fontSize: FontSize.size_2xs,
    fontFamily: FontFamily.helvetica,
    color: Color.colorWhite,
    textAlign: "left",
    position: "absolute",
  },
  ovalParent: {
    height: "48.48%",
    width: "55.17%",
    bottom: "51.52%",
    left: "44.83%",
    position: "absolute",
  },
  iconfontAwesomeFreesolidcParent: {
    height: "2.21%",
    width: "7.73%",
    top: "4.22%",
    right: "4.27%",
    bottom: "93.57%",
    left: "88%",
    position: "absolute",
  },
  logout: {
    left: 28,
    textAlign: "left",
    fontSize: FontSize.size_sm,
    top: 0,
    position: "absolute",
  },
  frameIcon: {
    width: 20,
    height: 20,
    top: 0,
    overflow: "hidden",
    left: 0,
  },
  logoutParent: {
    top: 360,
    left: 148,
    width: 79,
  },
  homeIndicator: {
    marginLeft: -66.5,
    bottom: 8,
    left: "50%",
    borderRadius: Border.br_81xl,
    backgroundColor: Color.tertiaryColor,
    width: 134,
    height: 5,
    position: "absolute",
  },
  homeIndicatorLight: {
    top: 1460,
    height: 34,
    left: 0,
    backgroundColor: Color.colorWhite,
  },
  screen3: {
    flex: 1,
    height: 1494,
    overflow: "hidden",
    width: "100%",
    backgroundColor: Color.colorWhite,

  },
});

export default Screen3;

import * as React from "react";
import { StyleSheet, Text, View ,Image } from "react-native";
import { FontFamily, Color, FontSize } from "../components/GlobalStyles";

const SectionCard = () => {
  return (
    <View style={styles.groupParent}>
      <Image
        style={styles.groupChild}
        contentFit="cover"
        source={require("../../assets/group-61.png")}
      />
      <Text style={[styles.imASoftware, styles.ptsTypo]}>
        I’m a software developer that has been in the crypto space since 2012.
        And I’ve seen it grow and falter over a period of time. Really happy to
        be here!
      </Text>
      <View style={styles.ptsWrapper}>
        <Text style={[styles.pts, styles.ptsTypo]}>6000 Pts</Text>
      </View>
      <View style={[styles.groupWrapper, styles.wrapperLayout]}>
        <View style={[styles.jinaSimonsWrapper, styles.wrapperLayout]}>
          <Text style={[styles.jinaSimons, styles.ptsTypo]}>Jina Simons</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  ptsTypo: {
    textAlign: "left",
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
    left: 0,
    position: "absolute",
  },
  wrapperLayout: {
    height: 17,
    width: 100,
    position: "absolute",
  },
  groupChild: {
    left: 134,
    width: 76,
    height: 75,
    top: 0,
    position: "absolute",
  },
  imASoftware: {
    top: 135,
    lineHeight: 20,
    color: Color.secondaryColro,
    textAlign: "left",
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    width: 343,
  },
  pts: {
    fontSize: FontSize.size_xs,
    color: Color.secondaryColro,
    textAlign: "left",
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
    top: 0,
  },
  ptsWrapper: {
    top: 112,
    left: 147,
    width: 70,
    height: 15,
    position: "absolute",
  },
  jinaSimons: {
    color: Color.primaryColor,
    textAlign: "left",
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    top: 0,
  },
  jinaSimonsWrapper: {
    left: 0,
    height: "200%",
    width:"100%",
    top: 0,
  },
  groupWrapper: {
    top: 87,
    left: 133,
  },
  groupParent: {
    top: 125,
    left: 16,
    height: 215,
    width: 343,
    position: "absolute",
  },
});

export default SectionCard;

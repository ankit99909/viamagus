import * as React from "react";

import { StyleSheet, Text, View ,Image} from "react-native";
import { FontFamily, FontSize, Color } from "../components/GlobalStyles";

const FormContainer = () => {
  return (
    <View style={styles.component1}>
      <Image
        style={[styles.component1Child, styles.iconsolidclockLayout]}
        contentFit="cover"
        source={require("../../assets/group-12376.png")}
      />
      <View style={styles.groupParent}>
        <View style={[styles.groupContainer, styles.groupPosition]}>
          <View style={[styles.groupWrapper, styles.groupWrapperPosition]}>
            <View style={[styles.groupFrame, styles.groupWrapperPosition]}>
              <View style={[styles.groupFrame, styles.groupWrapperPosition]}>
                <Image
                  style={[styles.iconsolidclock, styles.iconsolidclockLayout]}
                  contentFit="cover"
                  source={require("../../assets/iconsolidclock1.png")}
                />
                <Text style={[styles.text, styles.atTypo]}>03:23:12</Text>
                <Text style={[styles.startingIn, styles.startingInPosition]}>
                  Starting in
                </Text>
              </View>
            </View>
          </View>
          <Text style={[styles.underOrOver, styles.text2Typo]}>
            Under or Over
          </Text>
        </View>
        <View
          style={[
            styles.bitcoinPriceWillBeUnder2Wrapper,
            styles.groupWrapperPosition,
          ]}
        >
          <Text style={[styles.bitcoinPriceWillContainer, styles.textTypo]}>
            <Text
              style={[styles.bitcoinPriceWill, styles.atTypo]}
            >{`Bitcoin price will be under
`}</Text>
            <Text style={styles.at7AEtOn22ndJan21}>
              <Text style={styles.text1}>{`$24,524 `}</Text>
              <Text style={styles.atTypo}>at</Text>
              <Text style={styles.text2Typo}>{` `}</Text>
              <Text style={styles.atTypo}>7 a ET on 22nd Jan’21</Text>
            </Text>
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  iconsolidclockLayout: {
    maxHeight: "100%",
    overflow: "hidden",
    maxWidth: "100%",
    bottom: "0%",
    position: "absolute",
  },
  groupPosition: {
    right: "0%",
    top: "0%",
  },
  groupWrapperPosition: {
    bottom: "0%",
    position: "absolute",
  },
  atTypo: {
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
  },
  startingInPosition: {
    fontSize: FontSize.size_xs,
    top: "11.76%",
    left: "0%",
    position: "absolute",
  },
  text2Typo: {
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
  },
  textTypo: {
    fontSize: FontSize.size_sm,
    textAlign: "left",
    top: "0%",
    position: "absolute",
  },
  component1Child: {
    left: "0%",
    right: "0%",
    top: "0%",
    width: "100%",
    height: "100%",
  },
  iconsolidclock: {
    height: "82.35%",
    width: "9.46%",
    top: "17.65%",
    right: "41.22%",
    left: "49.32%",
  },
  text: {
    left: "64.19%",
    textAlign: "left",
    color: Color.colorPlum_100,
    fontSize: FontSize.size_sm,
    top: "0%",
    position: "absolute",
  },
  startingIn: {
    color: Color.colorPlum_200,
    textAlign: "right",
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
  },
  groupFrame: {
    left: "0%",
    right: "0%",
    top: "0%",
    width: "100%",
    height: "100%",
  },
  groupWrapper: {
    width: "47.59%",
    left: "52.41%",
    opacity: 0.75,
    right: "0%",
    top: "0%",
    height: "100%",
  },
  underOrOver: {
    textTransform: "uppercase",
    fontSize: FontSize.size_xs,
    top: "11.76%",
    left: "0%",
    position: "absolute",
    textAlign: "left",
    color: Color.colorPlum_100,
  },
  groupContainer: {
    height: "23.94%",
    bottom: "76.06%",
    left: "0%",
    width: "100%",
    position: "absolute",
  },
  bitcoinPriceWill: {
    color: Color.colorPlum_100,
  },
  text1: {
    fontWeight: "700",
    fontFamily: FontFamily.montserratBold,
  },
  at7AEtOn22ndJan21: {
    color: Color.colorWhite,
  },
  bitcoinPriceWillContainer: {
    lineHeight: 19,
    textAlign: "left",
    left: "0%",
    width: "100%",
  },
  bitcoinPriceWillBeUnder2Wrapper: {
    height: "53.52%",
    width: "75.31%",
    top: "46.48%",
    right: "26.69%",
    left: "0%",
  },
  groupParent: {
    height: "68.27%",
    width: "91.2%",
    top: "16.35%",
    right: "4.4%",
    bottom: "15.38%",
    left: "4.4%",
    position: "absolute",
  },
  component1: {
    top: 1,
    left: 1,
    borderStyle: "solid",
    borderColor: Color.colorBlack,
    width: 341,
    height: 104,
    position: "absolute",
  },
});

export default FormContainer;

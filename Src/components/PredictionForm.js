import  React ,{useState} from "react";
import { Text, StyleSheet, View, TouchableOpacity,Image } from "react-native";
import { Color, FontFamily, FontSize, Border } from "../components/GlobalStyles";
import SectionFilter from "./SectionFilter";
import Modal from 'react-native-modal';
import { useNavigation } from '@react-navigation/native';



const PredictionForm = () => {
  const [isModalVisible, setModalVisible] = useState(false);
  const navigation = useNavigation()

  const toggleModal = () => {
    setModalVisible(!isModalVisible);

  };

  return (
    <View style={styles.frameParent}>
      <View style={[styles.groupParent, styles.groupPosition]}>
        <View style={[styles.xParent, styles.parentPosition3]}>
          <Text style={[styles.x, styles.xFlexBox]}>3.25x</Text>
          <Text style={[styles.under, styles.parentPosition2]}>Under</Text>
        </View>
        <View style={[styles.xGroup, styles.parentPosition3]}>
          <Text style={[styles.x1, styles.xFlexBox]}>1.25x</Text>
          <Text style={[styles.under, styles.parentPosition2]}>Over</Text>
        </View>
        <View style={[styles.prizePoolParent, styles.parentPosition2]}>
          <Text style={[styles.under, styles.parentPosition2]}>Prize Pool</Text>
          <Text style={[styles.text, styles.textTypo]}>$12,000</Text>
        </View>
        <View style={styles.entryFeesParent}>
          <Text style={[styles.under, styles.parentPosition2]}>Entry Fees</Text>
          <View style={[styles.groupWrapper, styles.parentPosition1]}>
            <View style={[styles.parent, styles.parentPosition1]}>
              <Text style={[styles.text1, styles.textTypo]}>5</Text>
              <Image
                style={styles.icon}
                contentFit="cover"
                source={require("../../assets/254981.png")}
              />
            </View>
          </View>
        </View>
      </View>
      <View style={[styles.groupContainer, styles.groupPosition]}>
        <View style={[styles.groupFrame, styles.groupLayout1]}>
          <View style={[styles.groupFrame, styles.groupLayout1]}>
            <View style={[styles.groupChild, styles.groupLayout]} />
            <View style={[styles.fill5Parent, styles.parentPosition]}>
              <Image
                style={[styles.fill5Icon, styles.iconLayout]}
                contentFit="cover"
                source={require("../../assets/fill-51.png")}
              />
              <Text style={[styles.under1, styles.under1Typo]}>Under</Text>
            </View>
          </View>
        </View>
        <View style={[styles.groupView, styles.groupLayout1]}>
          <View style={[styles.groupFrame, styles.groupLayout1]}>
            <TouchableOpacity style={[styles.groupItem, styles.groupLayout]} onPress={()=>toggleModal()}/>
            <View style={[styles.fill6Parent, styles.parentPosition]}>
              <Image
                style={[styles.fill6Icon, styles.iconLayout]}
                contentFit="cover"
                source={require("../../assets/fill-61.png")}
              />
            
              <Text style={[styles.under1, styles.under1Typo]}>Over</Text>
             
            </View>
          </View>
        </View>
      </View>
      <Text style={[styles.whatsYourPrediction, styles.under1Typo]}>
        What’s your prediction?
      </Text>
      <Modal
      isVisible={isModalVisible}
      onBackdropPress={toggleModal}
      onBackButtonPress={toggleModal}
    >
      <View style={[styles.groupParent1, styles.groupLayout2]}>
        <View style={[styles.groupFrame, styles.groupLayout2]}>
          <View style={[styles.groupFrame, styles.groupLayout2]}>
            <View style={[styles.groupChild1, styles.groupLayout2]} />
            <View style={[styles.groupChild2, styles.groupChild2Bg]} />
          </View>
        </View>
        <View style={styles.groupParent2}>
          <View style={[styles.rectangleContainer, styles.groupChild3Layout]}>
          <TouchableOpacity style={[styles.groupChild3, styles.groupChild3Layout]} onPress={()=>navigation.navigate("BottomTab")}>
           
            <Text style={[styles.submitMyPrediction, styles.predictionTypo]}>
              Submit my prediction
            </Text>

            </TouchableOpacity>
          </View>
          <View style={styles.yourPredictionIsUnderWrapper}>
            <Text style={[styles.yourPredictionIs, styles.predictionTypo]}>
              Your Prediction is Under
            </Text>
          </View>
          <SectionFilter />
        </View>
      </View>
    </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  groupPosition: {
    height: 40,
    left: 0,
    position: "absolute",
  },
  parentPosition3: {
    bottom: "-5%",
    height: "105%",
    position: "absolute",
  },
  xFlexBox: {
    textAlign: "right",
    position: "absolute",
  },
  parentPosition2: {
    left: "0%",
    top: "0%",
  },
  textTypo: {
    textAlign: "left",
    left: "0%",
    color: Color.primaryColor,
    fontFamily: FontFamily.avenirNext,
    fontSize: FontSize.size_sm,
    position: "absolute",
  },
  parentPosition1: {
    bottom: "0%",
    right: "0%",
    position: "absolute",
  },
  groupLayout1: {
    width: 148,
    height: 40,
    top: 0,
    position: "absolute",
  },
  groupLayout: {
    borderWidth: 1.7,
    borderStyle: "solid",
    borderRadius: Border.br_26xl,
    width: 148,
    height: 40,
    left: 0,
    top: 0,
    position: "absolute",
  },
  parentPosition: {
    bottom: "30%",
    top: "27.5%",
    height: "42.5%",
    position: "absolute",
  },
  iconLayout: {
    borderRadius: Border.br_12xs_4,
    height: "66.43%",
    maxHeight: "100%",
    overflow: "hidden",
    maxWidth: "100%",
    position: "absolute",
  },
  under1Typo: {
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
    textAlign: "left",
    fontSize: FontSize.size_sm,
    position: "absolute",
  },
  x: {
    left: "8.7%",
    color: Color.primaryColor,
    fontFamily: FontFamily.avenirNext,
    fontSize: FontSize.size_sm,
    textAlign: "right",
    top: "54.76%",
  },
  under: {
    width:"102%",
     maxWidth:"120%",
    fontSize: FontSize.size_xs,
    textTransform: "uppercase",
    fontWeight: "500",
    fontFamily: FontFamily.montserratMedium,
    color: Color.tertiaryColor,
    textAlign: "right",
    position: "absolute",
  },
  xParent: {
    width: "14.79%",
    right: "54.02%",
    left: "31.19%",
    top: "0%",
  },
  x1: {
    left: "5.13%",
    color: Color.primaryColor,
    fontFamily: FontFamily.avenirNext,
    fontSize: FontSize.size_sm,
    textAlign: "right",
    top: "54.76%",
  },
  xGroup: {
    width: "12.54%",
    right: "32.15%",
    left: "55.31%",
    top: "0%",
  },
  text: {
    top: "54.76%",
  },
  prizePoolParent: {
    width: "24.76%",
    right: "75.24%",
    bottom: "-5%",
    height: "105%",
    position: "absolute",
  },
  text1: {
    top: "0%",
  },
  icon: {
    height: "68.42%",
    width: "43.33%",
    top: "10.53%",
    bottom: "21.05%",
    left: "56.67%",
    maxHeight: "100%",
    overflow: "hidden",
    maxWidth: "100%",
    right: "0%",
    position: "absolute",
  },
  parent: {
    height: "100%",
    width: "100%",
    left: "0%",
    top: "0%",
  },
  groupWrapper: {
    height: "45.24%",
    width: "39.47%",
    left: "60.53%",
    top: "54.76%",
  },
  entryFeesParent: {
    width: "24.44%",
    left: "75.56%",
    right: "0%",
    bottom: "-5%",
    top: "0%",
    height: "105%",
    position: "absolute",
  },
  groupParent: {
    width: 311,
    top: 0,
  },
  groupChild: {
    backgroundColor: Color.colorDarkslateblue,
    borderColor: Color.colorDarkslateblue,
  },
  fill5Icon: {
    width: "16.73%",
    top: "23.53%",
    right: "82.47%",
    bottom: "10.04%",
    left: "0.8%",
  },
  under1: {
    left: 15,
    color: Color.colorWhite,
    top: 0,
  },
  fill5Parent: {
    width: "40.68%",
    right: "29.49%",
    left: "29.83%",
  },
  groupFrame: {
    left: 0,
  },
  groupItem: {
    backgroundColor: Color.colorIndigo,
    borderColor: Color.colorIndigo,
  },
  fill6Icon: {
    width: "20.07%",
    top: "18.86%",
    right: "78.96%",
    bottom: "14.71%",
    left: "0.96%",
  },
  fill6Parent: {
    width: "33.9%",
    right: "32.88%",
    left: "33.22%",
  },
  groupView: {
    left: 165,
  },
  groupContainer: {
    top: 93,
    width: 313,
  },
  whatsYourPrediction: {
    top: 62,
    color: Color.secondaryColro,
    left: 0,
  },
  frameParent: {
    top: 125,
    left: 16,
    height: 133,
    width: 313,
    position: "absolute",
  },
  groupParent1: {
    top: 344,
    shadowColor: "rgba(0, 0, 0, 0.25)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 4,
    elevation: 4,
    shadowOpacity: 1,
  },
  groupLayout2: {
    height:850,
    width: 380,
    right:"2%"
  },
  groupChild1: {
    borderTopLeftRadius: Border.br_xl,
    borderTopRightRadius: Border.br_xl,
    borderWidth: 1,
    borderColor: Color.colorWhitesmoke_200,
    borderStyle: "solid",
    top: 0,
    backgroundColor: Color.colorWhite,
  },
  groupChild2: {
    top: 20,
    left: 172,
    borderRadius: 36,
    width: 30,
    height: 4,
  },
  groupChild2Bg: {
    backgroundColor: Color.tertiaryColor,
    position: "absolute",
  },
  groupParent2: {
    top: 44,
    height: 370,
    width: 343,
    left: 16,
    position: "absolute",
  },
  rectangleContainer: {
    top: 330,
  },
  groupChild3Layout: {
    height: 40,
    width: 343,
    left: 0,
    position: "absolute",
  },
  groupChild3: {
    borderRadius: 33,
    backgroundColor: Color.colorIndigo,
    top: 0,
  },
  groupParent2: {
    top: 44,
    height: 370,
    width: 343,
    left: 16,
    position: "absolute",
  },
  submitMyPrediction: {
    top: 12,
    left: 93,
    fontSize: FontSize.size_sm,
    color: Color.colorWhite,
  },
  yourPredictionIs: {
    color: Color.primaryColor,
    top: 0,
    fontSize: FontSize.size_base,
    left: 0,
  },
  predictionTypo: {
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
    textAlign: "left",
    position: "absolute",
  },
});

export default PredictionForm;

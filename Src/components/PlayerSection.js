import * as React from "react";
import { StyleSheet, View, Text , Image} from "react-native";

import { Color, FontFamily, FontSize, Border } from "../components/GlobalStyles";

const PlayerSection = () => {
  return (
    <View style={[styles.rectangleParent, styles.groupChildLayout]}>
      {/* <View style={[styles.groupChild, styles.groupChildLayout]} /> */}
     <View style={[styles.groupWrapper, styles.groupLayout1]}>
        <View style={[styles.groupParent, styles.groupLayout1]}>
          <View style={[styles.rectangleGroup, styles.groupLayout]}>
            <View style={[styles.groupItem, styles.groupLayout]} />
            <View style={[styles.groupInner, styles.groupLayout]} />
          </View>
          <View style={[styles.groupContainer, styles.groupPosition1]}>
            <View style={[styles.groupFrame, styles.groupPosition1]}>
              <View style={[styles.groupFrame, styles.groupPosition1]}>
                <View style={[styles.groupFrame, styles.groupPosition1]}>
                  <Text style={styles.predictedUnder}>232 predicted under</Text>
                </View>
              </View>
            </View>
            <View style={[styles.groupWrapper1, styles.groupWrapperLayout1]}>
              <View style={[styles.groupWrapper2, styles.groupWrapperLayout1]}>
                <View
                  style={[styles.groupWrapper2, styles.groupWrapperLayout1]}
                >
                  <Text style={styles.predictedOver}>123 predicted over</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={[styles.groupParent1, styles.groupPosition]}>
            <View style={[styles.groupWrapper3, styles.groupWrapperPosition]}>
              <View style={[styles.groupWrapper4, styles.groupWrapperPosition]}>
                <View
                  style={[styles.groupWrapper4, styles.groupWrapperPosition]}
                >
                  <View style={[styles.vectorParent, styles.parentPosition]}>
                    <Image
                      style={[styles.vectorIcon, styles.vectorIconPosition]}
                      contentFit="cover"
                      source={require("../../assets/vector2.png")}
                    />
                    <Text style={[styles.viewChart, styles.playersTypo]}>
                      View chart
                    </Text>
                  </View>
                  <View style={styles.iconfontAwesomeFreesolidc} />
                </View>
              </View>
            </View>
            <View style={[styles.groupWrapper5, styles.groupWrapperLayout]}>
              <View style={[styles.groupWrapper6, styles.groupWrapperLayout]}>
                <View style={[styles.groupWrapper6, styles.groupWrapperLayout]}>
                  <Text style={[styles.players, styles.playersTypo]}>
                    355 Players
                  </Text>
                <Image
                    style={[styles.vectorIcon1, styles.vectorIconPosition]}
                    contentFit="cover"
                    source={require("../../assets/vector2.png")}
                  />
                </View>
              </View>
            </View>
          </View>
        </View>
      </View> 
    </View>
  );
};

const styles = StyleSheet.create({
  groupChildLayout: {
    height: 110,
    width: 341,
    position: "absolute",
    borderTopLeftRadius: Border.br_9xs,
    borderTopRightRadius: Border.br_9xs,
    backgroundColor: Color.colorGhostwhite_100,
  },
  groupLayout1: {
    height: 69,
    width: 311,
    position: "absolute",
  },
  groupLayout: {
    height: 10,
    position: "absolute",
  },
  groupPosition1: {
    height: 15,
    left: 0,
    position: "absolute",
  },
  groupWrapperLayout1: {
    width: 150,
    height: 15,
    top: 0,
    position: "absolute",
  },
  groupPosition: {
    left: "0%",
    width: "100%",
  },
  groupWrapperPosition: {
    bottom: "0%",
    height: "100%",
    right: "0%",
    top: "0%",
    position: "absolute",
  },
  parentPosition: {
    right: "0%",
    position: "absolute",
  },
  vectorIconPosition: {
    maxHeight: "100%",
    maxWidth: "100%",
    top: "17.65%",
    overflow: "hidden",
    left: "0%",
    position: "absolute",
  },
  playersTypo: {
    color: Color.secondaryColro,
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
    fontSize: FontSize.size_sm,
    textAlign: "left",
    top: 0,
    position: "absolute",
  },
  groupWrapperLayout: {
    height: 17,
    width: 101,
    left: 0,
    position: "absolute",
  },
  groupChild: {
    top: 110,
    borderTopLeftRadius: Border.br_9xs,
    borderTopRightRadius: Border.br_9xs,
    backgroundColor: Color.colorGhostwhite_100,
    left: 0,
  },
  groupItem: {
    left: 233,
    borderTopRightRadius: Border.br_3xs,
    borderBottomRightRadius: Border.br_3xs,
    backgroundColor: Color.success,
    width: 78,
    top: 0,
  },
  groupInner: {
    borderTopLeftRadius: Border.br_3xs,
    borderBottomLeftRadius: Border.br_3xs,
    backgroundColor: Color.failuer,
    width: 233,
    top: 0,
    left: 0,
  },
  rectangleGroup: {
    top: 32,
    width: 311,
    left: 0,
  },
  predictedUnder: {
    textAlign: "left",
    color: Color.tertiaryColor,
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
    fontSize: FontSize.size_xs,
    top: 0,
    left: 0,
    position: "absolute",
  },
  groupFrame: {
    width: 160,
    top: 0,
  },
  predictedOver: {
    textAlign: "right",
    color: Color.tertiaryColor,
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
    fontSize: FontSize.size_xs,
    top: 0,
    left: 0,
    position: "absolute",
  },
  groupWrapper2: {
    left: 0,
  },
  groupWrapper1: {
    left: 201,
  },
  groupContainer: {
    top: 54,
    width: 311,
  },
  vectorIcon: {
    height: "73.53%",
    width: "16.18%",
    right: "83.82%",
    bottom: "8.82%",
  },
  viewChart: {
    left: 25,
  },
  vectorParent: {
    height: "85%",
    width: "89.57%",
    top: "5%",
    bottom: "10%",
    left: "10.43%",
  },
  iconfontAwesomeFreesolidc: {
    width: 20,
    height: 20,
    overflow: "hidden",
    top: 0,
    left: 0,
    position: "absolute",
  },
  groupWrapper4: {
    left: "0%",
    width: "100%",
    bottom: "0%",
    height: "100%",
  },
  groupWrapper3: {
    width: "36.98%",
    left: "63.02%",
  },
  players: {
    left: 20,
  },
  vectorIcon1: {
    height: "70.59%",
    width: "11.88%",
    right: "88.12%",
    bottom: "11.76%",
  },
  groupWrapper6: {
    top: 0,
  },
  groupWrapper5: {
    top: 1,
  },
  groupParent1: {
    height: "28.98%",
    bottom: "71.02%",
    right: "0%",
    position: "absolute",
    top: "0%",
    left: "0%",
    width: "100%",
  },
  groupParent: {
    top: 0,
    left: 0,
  },
  groupWrapper: {
    top: 20,
    left: 15,
  },
  rectangleParent: {
    top: 278,
    left: 1,
  },
  predictionTypo: {
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
    textAlign: "left",
    position: "absolute",
  },
  yourPredictionIsUnderWrapper: {
    width: 220,
    height: 20,
    top: 0,
    left: 0,
    position: "absolute",
  },
  yourPredictionIs: {
    color: Color.primaryColor,
    top: 0,
    fontSize: FontSize.size_base,
    left: 0,
  },
});

export default PlayerSection;

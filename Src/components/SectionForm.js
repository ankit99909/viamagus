import * as React from "react";
import { StyleSheet, View, Text,Image  } from "react-native";
import { Border, Color, FontSize, FontFamily } from "../components/GlobalStyles";

const SectionForm = () => {
  return (
    <View style={[styles.groupWrapper, styles.groupWrapperLayout]}>
      <View style={[styles.rectangleParent, styles.groupPosition1]}>
        <View style={[styles.groupChild, styles.groupPosition1]} />
        <Image
          style={styles.groupItem}
          contentFit="cover"
          source={require("../../assets/group-11345.png")}
        />
        <View style={styles.groupParent}>
          <View style={[styles.groupContainer, styles.groupPosition]}>
            <View
              style={[styles.underOrOverWrapper, styles.wrapperGroupPosition]}
            >
              <Text style={[styles.underOrOver, styles.textPosition]}>
                Under or Over
              </Text>
            </View>
            <View style={[styles.groupView, styles.groupPosition]}>
              <View style={[styles.wrapper, styles.wrapperGroupPosition]}>
                <Text style={[styles.text, styles.textPosition]}>{`81% `}</Text>
              </View>
              <Image
                style={[styles.groupInner, styles.groupPosition1]}
                contentFit="cover"
                source={require("../../assets/group-12234.png")}
              />
            </View>
          </View>
          <View style={[styles.groupParent1, styles.wrapperGroupPosition]}>
            <View style={[styles.top5Wrapper, styles.groupPosition]}>
              <Text style={[styles.underOrOver, styles.textPosition]}>
                Top 5
              </Text>
            </View>
            <View style={[styles.groupParent2, styles.wrapperGroupPosition]}>
              <View style={[styles.container, styles.wrapperGroupPosition]}>
                <Text
                  style={[styles.text, styles.textPosition]}
                >{`-51% `}</Text>
              </View>
              <Image
                style={[styles.groupIcon, styles.groupPosition1]}
                contentFit="cover"
                source={require("../../assets/group-12233.png")}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  groupWrapperLayout: {
    height: 119,
    width: 343,
  },
  groupPosition1: {
    left: 0,
    position: "absolute",
  },
  groupPosition: {
    left: "0%",
    position: "absolute",
  },
  wrapperGroupPosition: {
    right: "0%",
    position: "absolute",
  },
  textPosition: {
    textAlign: "left",
    left: "0%",
    top: "0%",
    position: "absolute",
  },
  groupChild: {
    top: 16,
    borderRadius: Border.br_8xs,
    backgroundColor: Color.colorWhite,
    borderStyle: "solid",
    borderColor: Color.colorGhostwhite_200,
    borderWidth: 1,
    height: 103,
    width: 343,
    left: 0,
  },
  groupItem: {
    height: "26.05%",
    width: "7.83%",
    right: "46.1%",
    bottom: "73.95%",
    left: "46.06%",
    maxWidth: "100%",
    overflow: "hidden",
    maxHeight: "100%",
    top: "0%",
    position: "absolute",
  },
  underOrOver: {
    fontSize: FontSize.size_sm,
    fontWeight: "600",
    fontFamily: FontFamily.montserratSemiBold,
    color: Color.secondaryColro,
  },
  underOrOverWrapper: {
    height: "26.98%",
    bottom: "73.02%",
    width: "120%",
    right: "0%",
    left: "0%",
    top: "0%",
  },
  text: {
    fontSize: FontSize.size_5xl,
    fontWeight: "700",
    fontFamily: FontFamily.montserratBold,
    color: Color.gray2,
  },
  wrapper: {
    height: "90.63%",
    width: "70.65%",
    bottom: "9.38%",
    left: "48.35%",
    top: "0%",
  },
  groupInner: {
    width: 32,
    height: 32,
    top: 0,
    left: 0,
  },
  groupView: {
    height: "50.79%",
    width: "89.22%",
    top: "49.21%",
    right: "10.78%",
    bottom: "0%",
  },
  groupContainer: {
    height: "100%",
    width: "40%",
    right: "60%",
    bottom: "0%",
    top: "0%",
  },
  top5Wrapper: {
    height: "27.42%",
    width: "50.63%",
    right: "59.38%",
    bottom: "72.58%",
    top: "0%",
  },
  container: {
    height: "93.55%",
    width: "70.25%",
    bottom: "6.45%",
    left: "43.75%",
    top: "0%",
  },
  groupIcon: {
    top: 1,
    width: 30,
    height: 30,
  },
  groupParent2: {
    height: "50%",
    top: "50%",
    width: "100%",
    right: "0%",
    left: "0%",
    bottom: "0%",
  },
  groupParent1: {
    height: "98.41%",
    width: "37.65%",
    bottom: "1.59%",
    left: "62.35%",
    top: "0%",
  },
  groupParent: {
    height: "52.94%",
    width: "74.34%",
    top: "30.25%",
    right: "19.83%",
    bottom: "16.81%",
    left: "5.83%",
    position: "absolute",
  },
  rectangleParent: {
    top: 0,
    left: 0,
    height: 119,
    width: 343,
  },
  groupWrapper: {
    top: 47,
    left: 16,
    position: "absolute",
  },
});

export default SectionForm;

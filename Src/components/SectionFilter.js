import * as React from "react";
import { Text, StyleSheet, View ,Image } from "react-native";

import { FontSize, FontFamily, Color } from "../components/GlobalStyles";

const SectionFilter = () => {
  return (
    <View style={[styles.groupWrapper, styles.groupWrapperLayout]}>
      <View style={[styles.entryTicketsParent, styles.groupWrapperLayout]}>
        <Text style={styles.entryTickets}>Entry tickets</Text>
        <View style={styles.rectangleParent}>
          <View style={[styles.groupChild, styles.groupPosition]} />
          <View style={[styles.groupContainer, styles.parentPosition]}>
            <View style={[styles.parent, styles.parentPosition]}>
              <Text style={styles.text}>14</Text>
              <Image
                style={[styles.icon, styles.iconPosition2]}
                contentFit="cover"
                source={require("../../assets/15.png")}
              />
              <Image
                style={[styles.icon1, styles.iconPosition1]}
                contentFit="cover"
                source={require("../../assets/16.png")}
              />
              <Image
                style={[styles.icon2, styles.iconPosition]}
                contentFit="cover"
                source={require("../../assets/17.png")}
              />
              <Image
                style={[styles.icon3, styles.iconPosition2]}
                contentFit="cover"
                source={require("../../assets/13.png")}
              />
              <Image
                style={[styles.icon4, styles.iconPosition1]}
                contentFit="cover"
                source={require("../../assets/12.png")}
              />
              <Image
                style={[styles.icon5, styles.iconPosition]}
                contentFit="cover"
                source={require("../../assets/11.png")}
              />
            </View>
          </View>
        </View>
        <View style={[styles.groupParent, styles.groupPosition]}>
          <View style={[styles.group, styles.groupPosition]}>
            <Text style={[styles.text1, styles.textTypo]}>$2000</Text>
            <Text style={[styles.youCanWinContainer, styles.totalTypo]}>
              <Text style={styles.youCanWin}>You can win</Text>
              <Text style={styles.text2}>{` `}</Text>
            </Text>
          </View>
          <View style={[styles.totalParent, styles.containerLayout]}>
            <Text style={[styles.total, styles.totalTypo]}>Total</Text>
            <View style={[styles.container, styles.containerLayout]}>
              <Text style={[styles.text3, styles.textTypo]}>5</Text>
              <Image
                style={styles.icon6}
                contentFit="cover"
                source={require("../../assets/25498.png")}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  groupWrapperLayout: {
    height: 254,
    width: 343,
    left: 0,
    position: "absolute",
  },
  groupPosition: {
    height: 36,
    left: 0,
    position: "absolute",
  },
  parentPosition: {
    width: 26,
    top: "50%",
    marginTop: -81.5,
    height: 163,
    position: "absolute",
  },
  iconPosition2: {
    opacity: 0.7,
    height: 15,
    left: 2,
    top: "50%",
    position: "absolute",
  },
  iconPosition1: {
    opacity: 0.4,
    height: 12,
    left: 2,
    top: "50%",
    position: "absolute",
  },
  iconPosition: {
    opacity: 0.2,
    height: 7,
    top: "50%",
    position: "absolute",
  },
  textTypo: {
    fontSize: FontSize.size_sm,
    textAlign: "left",
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
    position: "absolute",
  },
  totalTypo: {
    fontFamily: FontFamily.montserratMedium,
    fontWeight: "500",
    textAlign: "left",
    fontSize: FontSize.size_xs,
    left: 0,
    position: "absolute",
  },
  containerLayout: {
    height: 17,
    position: "absolute",
  },
  entryTickets: {
    textTransform: "uppercase",
    textAlign: "left",
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
    fontSize: FontSize.size_xs,
    color: Color.secondaryColro,
    top: 0,
    left: 0,
    position: "absolute",
  },
  groupChild: {
    top: 63,
    backgroundColor: Color.colorIndigo,
    opacity: 0.1,
    width: 343,
    height: 36,
  },
  text: {
    marginTop: -15.5,
    fontSize: FontSize.size_5xl,
    color: Color.primaryColor,
    top: "50%",
    textAlign: "left",
    fontFamily: FontFamily.montserratSemiBold,
    fontWeight: "600",
    left: 0,
    position: "absolute",
  },
  icon: {
    marginTop: 25.5,
    width: 22,
  },
  icon1: {
    marginTop: 52.5,
    width: 23,
  },
  icon2: {
    marginTop: 74.5,
    width: 22,
    left: 2,
    opacity: 0.2,
    height: 7,
  },
  icon3: {
    marginTop: -39.3,
    width: 21,
  },
  icon4: {
    marginTop: -64.5,
    width: 22,
  },
  icon5: {
    left: 5,
    width: 15,
    opacity: 0.2,
    height: 7,
    marginTop: -81.5,
  },
  parent: {
    left: 0,
  },
  groupContainer: {
    left: 159,
  },
  rectangleParent: {
    top: 35,
    height: 163,
    width: 343,
    left: 0,
    position: "absolute",
  },
  text1: {
    color: Color.colorSeagreen,
    top: 19,
    left: 0,
  },
  youCanWin: {
    color: Color.tertiaryColor,
  },
  text2: {
    color: Color.colorSeagreen,
  },
  youCanWinContainer: {
    top: 0,
  },
  group: {
    width: 80,
    top: 0,
  },
  total: {
    top: 2,
    color: Color.secondaryColro,
    fontWeight: "500",
  },
  text3: {
    left: 21,
    color: Color.primaryColor,
    top: 0,
  },
  icon6: {
    height: "76.47%",
    width: "43.33%",
    top: "11.76%",
    right: "56.67%",
    bottom: "11.76%",
    left: "0%",
    maxWidth: "100%",
    overflow: "hidden",
    maxHeight: "100%",
    position: "absolute",
  },
  container: {
    left: 38,
    width: 30,
    top: 0,
  },
  totalParent: {
    left: 275,
    width: 68,
    top: 19,
  },
  groupParent: {
    top: 218,
    width: 343,
    height: 36,
  },
  entryTicketsParent: {
    top: 0,
  },
  groupWrapper: {
    top: 48,
  },
});

export default SectionFilter;
